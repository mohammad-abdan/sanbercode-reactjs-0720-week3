import React from "react";
import DaftarBuah from "./tugas11/DaftarBuah"; //tugas 11
import Timer from "./tugas12/tugas12"; //tugas
// import DaftarPeserta from "./tugas13/DaftarPeserta"; //tugas 13

function App() {
  return (
    <div>
      {/* Tugas 11 */}
      <DaftarBuah />
      {/* Tugas 12 */}
      <Timer start={10} />
      {/* Tugas 13 */}
      {/* <DaftarPeserta /> */}
    </div>
  );
}

export default App;
