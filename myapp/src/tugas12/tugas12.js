import React, { Component } from "react";
// import Waktu from "./Waktu";
import "./tugas12.css";

class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0,
      date: new Date(),
    };
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start });
    }
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentDidUpdate() {
    // if (this.state.time === 0) {
    //   this.componentWillUnmount();
    // }
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date(),
    });
  }

  render() {
    return (
      <>
        {this.state.time >= 0 && (
          <div className="waktu">
            <>
              {/* <Waktu /> */}
              <p>Sekarang Jam : {this.state.date.toLocaleTimeString()}</p>
              <p>Hitung Mundur : {this.state.time}</p>
            </>
          </div>
        )}
      </>
    );
  }
}

export default Timer;
