import React, { Component } from "react";
import "./tugas13.css";

class Welcome extends Component {
  render() {
    return <h1>Daftar Peserta</h1>;
  }
}

class DaftarPeserta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataPeserta: ["Bambang", "Nita", "Adam", "Sobi"],
      inputName: "",
      indexOfFrom: -1,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleEdit(event) {
    let index = event.target.value;
    let peserta = this.state.dataPeserta[index];
    this.setState({ inputName: peserta, indexOfFrom: index });
  }

  handleDelete(event) {
    let index = event.target.value;
    let newPesertaLomba = this.state.dataPeserta;
    let editPeserta = newPesertaLomba[this.state.indexOfFrom];
    newPesertaLomba.splice(index, 1);
    if (editPeserta !== undefined) {
      var newIndex = newPesertaLomba.findIndex((el) => el === editPeserta);
      this.setState({ dataPeserta: newPesertaLomba, indexOfFrom: newIndex });
    } else {
      this.setState({ dataPeserta: newPesertaLomba });
    }
  }

  handleChange(event) {
    this.setState({ inputName: event.target.value });
  }

  handleSubmit(event) {
    //Menahan Submit
    event.preventDefault();
    let name = this.state.inputName;

    if (name.replace(/\s/g, "") !== "") {
      let newPesertaLomba = this.state.dataPeserta;
      let index = this.state.indexOfFrom;
      if (index === -1) {
        newPesertaLomba = [...newPesertaLomba, name];
      } else {
        newPesertaLomba[this.state.indexOfFrom] = name;
      }

      this.setState({
        dataPeserta: newPesertaLomba,
        inputName: "",
      });
    }
  }

  render() {
    return (
      <>
        <Welcome />
        <table>
          <thead>
            <tr>
              <td>No</td>
              <td>Nama</td>
              <td>Aksi</td>
            </tr>
          </thead>
          <tbody>
            {this.state.dataPeserta.map((val, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{val}</td>
                  <td>
                    <button onClick={this.handleEdit} value={index}>
                      Edit
                    </button>
                    &nbsp;
                    <button onClick={this.handleDelete} value={index}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>

        {/* Form */}
        <h1>Form Peserta</h1>
        <form onSubmit={this.handleSubmit}>
          <label>Masukkan nama peserta : </label>
          <input
            type="text"
            value={this.state.inputName}
            onChange={this.handleChange}
          />
          <button> Submit </button>
        </form>
      </>
    );
  }
}

export default DaftarPeserta;
