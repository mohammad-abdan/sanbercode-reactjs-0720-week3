import React from "react";
import Welcome from "./Welcome";
import ShowAge from "./ShowAge";

var person = [
  { name: "sarah", age: 25 },
  { name: "michael", age: 30 },
  { name: "john", age: 33 },
];

class UserInfo extends React.Component {
  render() {
    return (
      <>
        {person.map((x) => {
          return (
            <div style={{ border: "1px solid #efefef", padding: "20px" }}>
              <Welcome name={x.name} />
              <ShowAge age={x.age} />
            </div>
          );
        })}
      </>
    );
  }
}

export default UserInfo;
