import React, { Component } from "react";
import ItemBuah from "./ItemBuah";
import "./tugas11.css";

class Welcome extends Component {
  render() {
    return <h1>Harga Table Buah</h1>;
  }
}

class DaftarBuah extends Component {
  render() {
    let dataHargaBuah = [
      { nama: "Semangka", harga: 10000, berat: 1000 },
      { nama: "Anggur", harga: 40000, berat: 500 },
      { nama: "Strawberry", harga: 30000, berat: 400 },
      { nama: "Jeruk", harga: 30000, berat: 1000 },
      { nama: "Mangga", harga: 30000, berat: 500 },
    ];
    return (
      <>
        <Welcome />
        <table>
          <thead>
            <tr>
              <td>Nama</td>
              <td>Harga</td>
              <td>Berat</td>
            </tr>
          </thead>
          <tbody>
            {dataHargaBuah.map((x) => {
              return <ItemBuah item={x} />;
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default DaftarBuah;
